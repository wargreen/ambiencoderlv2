# Ambisonic Encoder

This encoder try to port the functions of the [NON-Mixer](https://github.com/original-male/non) encoder to a LV2 plug-in or stand-alone application. Build with  ([Faust](https://faust.grame.fr/))
For now we have :
- 2D & 3D Ambisonic encoding
- 2D & 3D send to a jconvolver's rev (early rev & lates)
- delay with distance
- Doppler FX with movement
- Mono or Stereo input

## Dependencies
Need my version of [HOA Librairie](https://framagit.org/wargreen/hoalibrary-faust)

## Usage

Load the lv2 in your prefered host.
Outputs of AmbiEnc :
1. Dry W
2. Dry X
3. Dry Y
4. Rev Lates
5. Early Reflection W
6. Early Reflection X
7. Early Reflection Y


Outputs of AmbiEnc3D :
1. Dry W
2. Dry X
3. Dry Y
4. Dry Z
5. Rev Lates
6. Early Reflection W
7. Early Reflection X
8. Early Reflection Y
9. Early Reflection Z

