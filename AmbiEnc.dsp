declare name "Ambisonic Encoder 2D";
declare version "1.0";
declare author "Wargreen";
declare license "GPL v3";
declare copyright "(c) Wargreen 2019";

import("stdfaust.lib");

ORD = 1;

radius = hslider("Radius [unit:Meters]", 1.0, 0.1, 50, 0.01) : si.smooth(ba.tau2pole(0.02));
azimut = (hslider("Azimut [unit:Degres]", 0, -180, 180, 0.01) -90) * (ma.PI/180) : si.smooth(ba.tau2pole(0.02));
stereo = checkbox("Stereo input");
width = hslider("Stereo Width [unit:Degres]", 90, -180, 180, 0.01) * (ma.PI/180) : si.smooth(ba.tau2pole(0.02));
erLvl = hslider("Early Reflection Lvl [unit:dB]", 0, -20, 0, 0.01) : si.smooth(ba.tau2pole(0.02));


delay = de.fdelayltv(3,ma.SR, ma.SR * 1/340*radius : int);
filter = _ : fi.highshelf(1, (-0.43*radius) + 0.43, 15000);

volume(r) = 1. / (r * r * (r > 0.1) + (r < 0.1));
stereAzim(ch) = ma.neg( ba.if(stereo, ba.if(ch < 1, azimut-width/2, azimut+width/2), azimut));

encodedry(ch) = _ * volume(radius) <: ho.encoder(ORD, _, stereAzim(ch) ) : _ * ba.db2linear(-3),_,_;
encoderev(ch) = _ * ba.db2linear(erLvl) <: ho.encoder(ORD, _,  stereAzim(ch)) : _ * ba.db2linear(-3), _, _;


addout =  _,_;
dryprocess =  par(i, 2, encodedry(i)) :> _, par(i, ORD, addout);

revlateprocess = _,_ :>_;
revearlyprocess = par(i, 2, encoderev(i)) :> _, par(i, ORD, addout); 

process =_, _ : par(i, 2,delay : filter) <: dryprocess, revlateprocess, revearlyprocess;
